//sélection des différents éléments HTML à utiliser
const form = document.querySelector("form[name='weatherForm']");
const latitudeInput = document.querySelector("#latitude");
const longitudeInput = document.querySelector("#longitude");
const weatherDiv = document.querySelector("#weather");
//Ecoute de l'évènement submit sur le formulaire+ annulation  du rechargement de la page par défaut
form.addEventListener("submit", async event => {
  event.preventDefault();
//Récupération de la valeurs des inputs latitude et longitude
  const latitude = latitudeInput.value;
  const longitude = longitudeInput.value;
/*Requête à l'API météo avec les valeurs de latitude+longitude renseignées.
Await pour attendre la réponse et s'assurer de la réception des données JSON avec la méthode'json'*/
  try {
    const response = await fetch(
      `https://api.open-meteo.com/v1/forecast?latitude=${latitude}&longitude=${longitude}&current_weather=true&hourly=temperature_2m,relativehumidity_2m,windspeed_10m`
    );
    const data = await response.json();
/*Affichage des données météo sur la page, utilisation de l'objet weatherDiv pour l'accès à un élément Html
 + utilisation de la propriété innerHTML pour définir le contenu de l'élément*/
    weatherDiv.innerHTML = `
    <p>Température actuelle: ${data.current_weather.temperature}</p>
    <p>Vitesse du vent: ${data.current_weather.windspeed}</p>
    <p>Orientation du vent: ${data.current_weather.winddirection}</p>
  `;
  } catch (error) {
    weatherDiv.innerHTML = "<p>Données météorologiques non disponibles</p>";
    console.error(error);
  }
});